package bai2;

public class Point {
    private String name;
    private double xcoordinate;
    private double ycoordinate;
    public void display(){
        System.out.println("Point name: " +this.name);
        System.out.println("Point coordinate: " +this.xcoordinate +","+this.ycoordinate);
    }
    public Point(){
        this.name = "defautl";
        this.xcoordinate = 0;
        this.ycoordinate = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getXcoordinate() {
        return xcoordinate;
    }

    public void setXcoordinate(double xcoordinate) {
        this.xcoordinate = xcoordinate;
    }

    public double getYcoordinate() {
        return ycoordinate;
    }

    public void setYcoordinate(double ycoordinate) {
        this.ycoordinate = ycoordinate;
    }
     public Point(String name, double xcoordinate, double ycoordinate) {
        super();
        this.name = name;
        this.xcoordinate = xcoordinate;
        this.ycoordinate = ycoordinate;
     }

     public void Distance(String name2, double a,double b) {
        double distance = Math.sqrt((this.xcoordinate-a)*(this.xcoordinate-a)+(this.ycoordinate-b)*(this.ycoordinate-b));
         System.out.println("Khoang cach giua "+this.name +" va "+name2+" la: "+distance);
     }

}
