package com.bank;

public class Account {
    private String name;
    private int balance;
    private boolean isVip;
    private String screatcode;
    //method display account info
    public void display(){
        System.out.println("Account name: " + this.name);
        System.out.println("Balance: " + this.balance);
    }
    //method for name attribute

    public String getName() {
        return this.name;
    }

    public void setName(String pName) {
        this.name = pName;
    }
    //method for balance attribute
    public int getBalance() {
        return this.balance;
    }

    public void setBalance(int balance) {
        if (balance>0)
            this.balance = balance;
        else{
            System.out.println("so du tai khoan phai >= 0");
            this.balance =0;
        }
    }

    public boolean isVip() {
        return isVip;
    }

    public void setVip(boolean vip) {
        isVip = vip;
    }

    public String getScreatcode() {
        return screatcode;
    }

    public void setScreatcode(String screatcode) {
        this.screatcode = screatcode;
    }
// constructor
    public Account(){
        this.name = "default";
        this.balance = 0;
    }

    public Account(String name, int balance) {
        super();
        this.name = name;
        this.balance = balance;
    }

    //write deposit and withdraw method
    public void deposit(int value) {
        if (value > 0) {
        this.balance += value;
            System.out.println("Nap tien thanh cong!");
    }
    }
    public boolean withdraw(int value){
        if (value > 0){
            int temp = balance - value;
            if (temp >= 0){
                balance -= value;
                System.out.println("Rut tien thanh cong");
                return true;
            }
            else {
                System.out.println("Rut tien khong thanh cong!");
                return false;
            }
        }
        else {
            System.out.println("Rut tien khong thanh cong!");
            return false;
        }
    }


}
