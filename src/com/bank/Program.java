package com.bank;

public class Program {
    public static void main(String[] args) {
        Account a1 = new Account();
        a1.display();
        Account a2 = new Account("Mr Hung",2000);
        a2.display();
        a2.deposit(10000);
        a2.display();
        a2.withdraw(15000);
        a2.display();
        a2.withdraw(12000);
        a2.display();
    }
}
